/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#undef DEBUG

#include <linux/kernel.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/iopoll.h>
#include <linux/module.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/spi-nor.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/io.h>
#include <linux/version.h>

#include <dct-quadspi-reg.h>

#define SPI_NOR_MAX_ID_LEN	6

#define ADDR_BCNT4 BIT(21)
#define ADDR_BCNT3 BIT(20)
#define SPI_FLASH_FIFO_REG 0x0

#define DELAY_US 1000
#define TIMEOUT_US 100000

#define MODEA_MASK \
	(RDWR_MASK | ADDR_BCNT_MASK | DUMMY_CCNT_MASK | \
	 COMMAND_MASK | CMD_DATAIO_MASK | ADDR_DATAIO_MASK)

#define QSPI_MAX_CHIPSELECT	4

#define dct_err(qspi, fmt, arg...) \
	dev_err(qspi->dev, "%s: " fmt, __func__, ##arg);
#define dct_warn(qspi, fmt, arg...) \
	dev_warn(qspi->dev, "%s: " fmt, __func__, ##arg);
#define dct_dbg(qspi, fmt, arg...) \
	dev_dbg(qspi->dev, "%s: " fmt, __func__, ##arg);

struct dct_qspi;

struct dct_qspi_flash {
	struct spi_nor nor;
	struct dct_qspi *qspi;
	u32 cs;
	bool registered;
};

struct dct_qspi {
	struct platform_device  *pdev;
	struct device *dev;
	struct clk *sclk;
	struct clk *fclk;
	void __iomem *fifo_base;
	void __iomem *ctrl_base;
	struct dct_qspi_flash qspi_flash[QSPI_MAX_CHIPSELECT];
};

static int dct_qspi_wait_done(struct dct_qspi *qspi)
{
	u32 stat;
	int ret;

	ret = readl_poll_timeout(qspi->ctrl_base + SPI_STATUS_REG, stat,
				 !(stat & BUSY_MASK), DELAY_US, TIMEOUT_US);
	if (ret)
		dct_warn(qspi, "busy timed out\n");

	return ret;
}

static int dct_qspi_wait_flush(struct dct_qspi *qspi)
{
	u32 stat;
	int ret;

	ret = readl_poll_timeout(qspi->ctrl_base + SPI_STATUS_REG, stat,
				 !(stat & FIFO_FLUSH_BUSY_MASK), DELAY_US, TIMEOUT_US);
	if (ret)
		dct_warn(qspi, "flush busy timed out\n");

	return ret;
}

static int dct_qspi_rx_avail(struct dct_qspi *qspi)
{
	u32 stat;
	int ret;

	ret = readl_poll_timeout(qspi->ctrl_base + SPI_STATUS_REG, stat,
				 (stat & FIFO_RX_AVAIL_MASK), DELAY_US, TIMEOUT_US);
	if (ret)
		dct_warn(qspi, "rx avail timed out (status 0x%x)\n", stat);

	return ret;
}

static int dct_qspi_tx_avail(struct dct_qspi *qspi)
{
	u32 stat;
	int ret;

	ret = readl_poll_timeout(qspi->ctrl_base + SPI_STATUS_REG, stat,
				 (stat & FIFO_TX_AVAIL_MASK), DELAY_US, TIMEOUT_US);
	if (ret)
		dct_warn(qspi, "tx avail timed out (status 0x%x)\n", stat);

	return ret;
}

static int dct_qspi_reset(struct dct_qspi *qspi)
{
	dct_dbg(qspi, "\n");

	// dct_qspi_cs_activate(qspi, 0x03);

	return 0;
}

static int dct_spi_flush(struct dct_qspi *qspi)
{
	int ret;
	u32 ctrl, status;

	ctrl = readl(qspi->ctrl_base + SPI_CTRL_REG);
	ctrl |= FIFO_FLUSH_MASK;
	writel(ctrl, qspi->ctrl_base + SPI_CTRL_REG);
	ctrl &= ~FIFO_FLUSH_MASK;
	writel(ctrl, qspi->ctrl_base + SPI_CTRL_REG);

	ret = dct_qspi_wait_flush(qspi);
	if (ret)
		return -1;

	status = readl(qspi->ctrl_base + SPI_STATUS_REG);
	if (status & FIFO_RX_AVAIL_MASK) {
		dct_dbg(qspi, "can't flush, rx fifo not empty (%08x)\n", status);
		return -1;
	}

	if ((status & FIFO_TX_AVAIL_MASK) == 0) {
		dct_dbg(qspi, "can't flush, tx fifo not free (%08x)\n", status);
		return -1;
	}

	return ret;
}

static void dct_qspi_cs_activate(struct dct_qspi *qspi, u8 cs)
{
	u32 mode_a;

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~CSEL_MASK;
	mode_a |= (cs << CSEL_SHIFT);
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);
}

static void dct_spi_start(struct dct_qspi *qspi)
{
	u32 ctrl;

	ctrl = readl(qspi->ctrl_base + SPI_CTRL_REG);
	writel(ctrl | START_MASK, qspi->ctrl_base + SPI_CTRL_REG);
	writel(ctrl, qspi->ctrl_base + SPI_CTRL_REG);
}

static int dct_spi_read_chunk(struct dct_qspi *qspi, u8 *ptr, const u32 len)
{
	u32 k, i = len;
	u32 tmp;

	while (ptr && i) {
		if (dct_qspi_rx_avail(qspi))
			return -1;

		tmp = 0;
		k = (i > 4) ?  4 : i;

		tmp = readl(qspi->fifo_base + SPI_FLASH_FIFO_REG);
		memcpy(ptr, &tmp, k);
		ptr += k;
		i -= k;
	}

	return 0;
}

static int dct_spi_write_chunk(struct dct_qspi *qspi, const u8 *ptr, const u32 len)
{
	u32 k, i = len;
	u32 tmp;

	while (ptr && i > 0) {
		k = i > 4 ? 4 : i;

		if (dct_qspi_tx_avail(qspi)) {
			dct_dbg(qspi, "len: %d\n", i);
			return -1;
		}

		memcpy(&tmp, ptr, sizeof(tmp));
		writel(tmp, qspi->fifo_base + SPI_FLASH_FIFO_REG);

		ptr += k;
		i -= k;
	}

	return 0;
}

static int dct_qspi_read_reg(struct spi_nor *nor, u8 opcode, u8 *buf, size_t len)
{
	int ret;
	struct dct_qspi_flash *qspi_flash = nor->priv;
	struct dct_qspi *qspi = qspi_flash->qspi;
	u32 mode_a;

	dct_dbg(qspi, "cs %d, opcode=%x, len=%zu\n",
			   qspi_flash->cs, opcode, len);

	dct_qspi_cs_activate(qspi, qspi_flash->cs);
	dct_spi_flush(qspi);

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~MODEA_MASK;
	mode_a |= opcode << COMMAND_SHIFT;
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

	writel(len, qspi->ctrl_base + SPI_XFER_CNT_REG);

	dct_spi_start(qspi);

	ret = dct_spi_read_chunk(qspi, buf, len);

	if (dct_qspi_wait_done(qspi))
		ret = -1;

	dct_qspi_cs_activate(qspi, 0x03);
	dct_spi_flush(qspi);

	return ret;
}

static int dct_qspi_write_reg(struct spi_nor *nor, u8 opcode, const u8 *buf, size_t len)
{
	struct dct_qspi_flash *qspi_flash = nor->priv;
	struct dct_qspi *qspi = qspi_flash->qspi;
	u32 mode_a;
	int ret = 0;

	dct_dbg(qspi, "cs %d, opcode=%x, len=%zu\n",
			   qspi_flash->cs, opcode, len);

	dct_qspi_cs_activate(qspi, qspi_flash->cs);
	dct_spi_flush(qspi);

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~MODEA_MASK;
	mode_a |= opcode << COMMAND_SHIFT;
	if (len)
		mode_a |= RDWR_MASK;
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

	writel(len, qspi->ctrl_base + SPI_XFER_CNT_REG);

	dct_spi_start(qspi);

	ret = dct_spi_write_chunk(qspi, buf, len);

	if (dct_qspi_wait_done(qspi))
		ret = -1;

	dct_qspi_cs_activate(qspi, 0x03);
	dct_spi_flush(qspi);

	return ret;
}

static ssize_t dct_qspi_read(struct spi_nor *nor, loff_t from, size_t len,
			  u_char *buf)
{
	struct dct_qspi_flash *qspi_flash = nor->priv;
	struct dct_qspi *qspi = qspi_flash->qspi;
	u32 mode_a;
	int ret = 0;

	dct_dbg(qspi, "cs: %d from=%llx len=%zd opcode=0x%x dummy=%d addr_nbytes=%d\n",
			   qspi_flash->cs,
			   from, len,
			   nor->read_opcode,
			   nor->read_dummy,
			   nor->addr_nbytes);

	dct_qspi_cs_activate(qspi, qspi_flash->cs);
	dct_spi_flush(qspi);

	writel(from, qspi->ctrl_base + SPI_ADDR_REG);

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~MODEA_MASK;
	mode_a |= nor->read_opcode << COMMAND_SHIFT;
	mode_a |= (nor->read_dummy << DUMMY_CCNT_SHIFT);
	if (nor->addr_nbytes == 3)
		mode_a |= ADDR_BCNT3;
	if (nor->addr_nbytes == 4)
		mode_a |= ADDR_BCNT4;
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

	writel(len, qspi->ctrl_base + SPI_XFER_CNT_REG);

	dct_spi_start(qspi);

	ret = dct_spi_read_chunk(qspi, buf, len);

	if (dct_qspi_wait_done(qspi))
		ret = -1;

	dct_qspi_cs_activate(qspi, 0x03);
	dct_spi_flush(qspi);

	return len;
}

static ssize_t dct_qspi_write(struct spi_nor *nor, loff_t to, size_t len,
			    const u_char *buf)
{
	int ret;
	struct dct_qspi_flash *qspi_flash = nor->priv;
	struct dct_qspi *qspi = qspi_flash->qspi;
	u32 mode_a;

	dct_dbg(qspi, "-%d to=%llx len=%zd opcode=0x%x addr_nbytes=%d\n",
			   qspi_flash->cs,
			   to, len,
			   nor->program_opcode,
			   nor->addr_nbytes);


	dct_qspi_cs_activate(qspi, qspi_flash->cs);
	dct_spi_flush(qspi);

	writel(to, qspi->ctrl_base + SPI_ADDR_REG);

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~MODEA_MASK;
	mode_a |= nor->program_opcode << COMMAND_SHIFT;
	mode_a |= RDWR_MASK;
	if (nor->addr_nbytes == 3)
		mode_a |= ADDR_BCNT3;
	if (nor->addr_nbytes == 4)
		mode_a |= ADDR_BCNT4;
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

	writel(len, qspi->ctrl_base + SPI_XFER_CNT_REG);

	dct_spi_start(qspi);

	ret = dct_spi_write_chunk(qspi, buf, len);

	if (dct_qspi_wait_done(qspi))
		ret = -1;

	dct_qspi_cs_activate(qspi, 0x03);
	dct_spi_flush(qspi);

	return len;
}

static int dct_qspi_erase(struct spi_nor *nor, loff_t offs)
{
	struct dct_qspi_flash *qspi_flash = nor->priv;
	struct dct_qspi *qspi = qspi_flash->qspi;
	u32 mode_a;
	int ret = 0;

	dct_dbg(qspi, "-%d opcode=%x offs=0x%llx\n",
			   qspi_flash->cs,
			   nor->erase_opcode, offs);

	dct_qspi_cs_activate(qspi, qspi_flash->cs);
	dct_spi_flush(qspi);

	writel(offs, qspi->ctrl_base + SPI_ADDR_REG);

	mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);
	mode_a &= ~MODEA_MASK;
	mode_a |= nor->erase_opcode << COMMAND_SHIFT;
	if (nor->addr_nbytes == 3)
		mode_a |= ADDR_BCNT3;
	if (nor->addr_nbytes == 4)
		mode_a |= ADDR_BCNT4;
	writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

	writel(0, qspi->ctrl_base + SPI_XFER_CNT_REG);

	dct_spi_start(qspi);

	if (dct_qspi_wait_done(qspi))
		ret = -1;

	dct_qspi_cs_activate(qspi, 0x03);
	dct_spi_flush(qspi);

	return ret;
}

static const struct spi_nor_controller_ops dct_qspi_controller_ops = {
	.read_reg  = dct_qspi_read_reg,
	.write_reg = dct_qspi_write_reg,
	.read  = dct_qspi_read,
	.write = dct_qspi_write,
	.erase = dct_qspi_erase,
};

static int dct_qspi_setup_flash(struct dct_qspi *qspi,
				 struct device_node *np)
{
	struct spi_nor_hwcaps hwcaps = {
		.mask = SNOR_HWCAPS_READ |
			SNOR_HWCAPS_READ_FAST |
			SNOR_HWCAPS_PP,
	};
	struct platform_device *pdev = qspi->pdev;
	struct device *dev = &pdev->dev;
	u32 mode_a, property, i;
	u16 mode = 0;
	int ret = -1;
	unsigned int cs;
	struct dct_qspi_flash *qspi_flash;

	/* Get flash device data */
	for_each_available_child_of_node(dev->of_node, np) {

		if (of_property_read_u32(np, "reg", &cs)) {
			dev_err(dev, "Couldn't determine chip select.\n");
			goto err;
		}

		if (cs > QSPI_MAX_CHIPSELECT) {
			dev_err(dev, "Chip select %d out of range.\n", cs);
			goto err;
		}

		if (!of_property_read_u32(np, "spi-rx-bus-width", &property)) {
			switch (property) {
			case 1:
				break;
			case 2:
				mode |= SPI_RX_DUAL;
				break;
			case 4:
				mode |= SPI_RX_QUAD;
				break;
			default:
				dev_err(qspi->dev, "unsupported rx-bus-width\n");
				return -EINVAL;
			}
		}

		if (of_find_property(np, "spi-cpha", NULL))
			mode |= SPI_CPHA;

		if (of_find_property(np, "spi-cpol", NULL))
			mode |= SPI_CPOL;

		mode_a = readl(qspi->ctrl_base + SPI_MODEA_REG);

#if 0
		mode_a |= CLOCK_SELECT_MASK;
		mode_a &= ~(DUALIO_MASK | QUADIO_MASK);
		if (mode & SPI_RX_DUAL) {
			mode_a |= DUALIO_MASK;
			hwcaps.mask |= SNOR_HWCAPS_READ_1_1_2;
		} else if (mode & SPI_RX_QUAD) {
			mode_a |= QUADIO_MASK;
			hwcaps.mask |= SNOR_HWCAPS_READ_1_1_4;
		}
#endif
#if 0
		switch (mode & SPI_MODE_X_MASK) {
		case SPI_MODE_0:
			ctrl &= ~QSPI_CTRL_MODE3;
			break;
		case SPI_MODE_3:
			ctrl |= QSPI_CTRL_MODE3;
			break;
		default:
			dev_err(qspi->dev, "only mode 0 and 3 supported\n");
			return -EINVAL;
		}
#endif

		writel(mode_a, qspi->ctrl_base + SPI_MODEA_REG);

		qspi_flash = &qspi->qspi_flash[cs];
		qspi_flash->qspi = qspi;
		qspi_flash->cs = cs;

		qspi_flash->nor.dev   = qspi->dev;
		spi_nor_set_flash_node(&qspi_flash->nor, np);
		qspi_flash->nor.priv  = qspi_flash;
		qspi_flash->nor.controller_ops = &dct_qspi_controller_ops;

		ret = spi_nor_scan(&qspi_flash->nor, NULL, &hwcaps);
		if (ret) {
			dev_err(qspi->dev, "device scan failed\n");
			goto err;
		}

		qspi_flash->nor.mtd.name = devm_kasprintf(dev,
						GFP_KERNEL, "%s.%d",
						dev_name(dev), cs);
		if (!qspi_flash->nor.mtd.name) {
			ret = -ENOMEM;
			goto err;
		}

		ret = mtd_device_register(&qspi_flash->nor.mtd, NULL, 0);
		if (ret) {
			dev_err(qspi->dev, "mtd device parse failed\n");
			goto err;
		}

		qspi_flash->registered = true;
	}

	return 0;

err:
	for (i = 0; i < QSPI_MAX_CHIPSELECT; i++)
		if (qspi->qspi_flash[i].registered)
			mtd_device_unregister(&qspi->qspi_flash[i].nor.mtd);

	return ret;
}

static int dct_qspi_probe(struct platform_device *pdev)
{
	struct device_node *flash_np;
	struct dct_qspi *qspi;
	struct resource *res;
	int ret;

	qspi = devm_kzalloc(&pdev->dev, sizeof(*qspi), GFP_KERNEL);
	if (!qspi)
		return -ENOMEM;

	qspi->pdev = pdev;
	qspi->dev = &pdev->dev;

	qspi->sclk = devm_clk_get_enabled(&pdev->dev, "qspi_slow_clk");
	qspi->fclk = devm_clk_get_enabled(&pdev->dev, "qspi_fast_clk");

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo");
	if (res == NULL) {
		dev_err(&pdev->dev, "missing platform data\n");
		return -ENODEV;
	}
	qspi->fifo_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(qspi->fifo_base)) {
		dev_err(&pdev->dev, "can't map ctrl base\n");
		return PTR_ERR(qspi->fifo_base);
	}

	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "ctrl");
	if (res == NULL) {
		dev_err(&pdev->dev, "missing platform data\n");
		return -ENODEV;
	}
	qspi->ctrl_base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(qspi->ctrl_base)) {
		dev_err(&pdev->dev, "can't map ctrl base\n");
		return PTR_ERR(qspi->ctrl_base);
	}

	platform_set_drvdata(pdev, qspi);

	dct_qspi_reset(qspi);

	flash_np = of_get_next_available_child(pdev->dev.of_node, NULL);
	if (!flash_np) {
		dev_err(&pdev->dev, "no SPI flash device to configure\n");
		ret = -ENODEV;
		goto err;
	}

	ret = dct_qspi_setup_flash(qspi, flash_np);
	of_node_put(flash_np);
	if (ret) {
		dev_err(&pdev->dev, "unable to setup flash chip\n");
		goto err;
	}

	return 0;

err:

	return ret;
}

static int dct_qspi_remove(struct platform_device *pdev)
{
	struct dct_qspi *qspi = platform_get_drvdata(pdev);
	int i;

	for (i = 0; i < QSPI_MAX_CHIPSELECT; i++)
		if (qspi->qspi_flash[i].registered)
			mtd_device_unregister(&qspi->qspi_flash[i].nor.mtd);

	return 0;
}

static const struct of_device_id dct_qspi_match[] = {
	{.compatible = "dct,dct-qspi"},
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, dct_qspi_match);

static struct platform_driver dct_qspi_driver = {
	.probe	= dct_qspi_probe,
	.remove	= dct_qspi_remove,
	.driver	= {
		.name = "dct-qspi",
		.of_match_table = dct_qspi_match,
	},
};
module_platform_driver(dct_qspi_driver);

MODULE_DESCRIPTION("DCT SPI Flash Interface driver");
MODULE_LICENSE("GPL v2");
